package fr.cnam.foad.nfa035.badges.service.impl;

import fr.cnam.foad.nfa035.badges.service.BadgesWalletRestService;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Set;
import java.util.SortedMap;

/**
 * Commentez-moi
 * Partie controlleur du sercice REST avec une implémentation du CRUD
 */
@RestController
public class BadgesWalletRestServiceImpl implements BadgesWalletRestService {

    @Qualifier("jsonBadge")
    @Autowired
    JSONBadgeWalletDAO jsonBadgeDao;


    /**
     * {@inheritDoc}
     *
     * @param badge le badge à écrire, ou plutôt ses métadonnées
     * @param file  le fichier image du badge à écrire
     * @return ResponseEntity la réponse REST toujours
     */
    @Override
    public ResponseEntity putBadge(@RequestPart DigitalBadge badge, @RequestPart MultipartFile file, HttpServletRequest request) throws ServletException {
        try {
            SortedMap<DigitalBadge, DigitalBadgeMetadata> badges = jsonBadgeDao.getWalletMetadataMap();
            DigitalBadgeMetadata meta = new DigitalBadgeMetadata();
            meta.setImageSize(file.getSize());
            badge.setMetadata(meta);
            //System.out.println(meta.toString());
            // SI le content-type est image/jpeg, par exemple, alors le nom du fichier sera jpeg, et par la suite, on aura bien le bon mime-type ...;)

            if (badges.containsKey(badge)) {
                // logique de suppression puis de création ...
                jsonBadgeDao.removeBadge(badge);
                jsonBadgeDao.addBadge(badge, file.getInputStream());

            }

            badge.setMetadata(meta);
            badge.setBadge(new File(request.getPart("file").getContentType()));
            System.out.println(badge.getMetadata());
            System.out.println(badge.getSerial());
            jsonBadgeDao.addBadge(badge, file.getInputStream());
            return ResponseEntity.ok().build();
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param badge le badge à lire
     * @return ResponseEntity la réponse REST toujours
     */
    @Override
    public ResponseEntity<StreamingResponseBody> readBadge(@RequestBody DigitalBadge badge, @RequestParam(required = false) boolean attachment) {

        StreamingResponseBody responseBody = outputStream -> {
            jsonBadgeDao.getBadgeFromMetadata(outputStream, badge);
        };
        // Il va falloire ajouter le MIME-TYPE de l'image dans les métadonnées du badge à un moment
        String imageType = badge.getMetadata().getImageType();
        MediaType mimeType = imageType == null ? MediaType.valueOf("image/png") : MediaType.valueOf(imageType);
        String fileName = badge.getSerial();
        HttpHeaders responseHeaders = new HttpHeaders();
        if (attachment) {
            responseHeaders.add("content-disposition", "attachment; filename=" + fileName);
        }
        return ResponseEntity.ok().headers(responseHeaders).contentType(mimeType)
                .body(responseBody);

    }

    /**
     * {@inheritDoc}
     *
     * @return ResponseEntity la réponse REST toujours
     */
    @Override
    public ResponseEntity<Set<DigitalBadge>> getMetadata() {
        try {
            return ResponseEntity.ok().body(jsonBadgeDao.getWalletMetadata().getAllBadges());
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param badge le badge à supprimer
     * @return ResponseEntity la réponse REST toujours
     */
    @Override
    public ResponseEntity deleteBadge(@RequestBody DigitalBadge badge) {
        try {
            SortedMap<DigitalBadge, DigitalBadgeMetadata> badges = jsonBadgeDao.getWalletMetadataMap();
            if (badges.containsKey(badge)) {
                jsonBadgeDao.removeBadge(badge);
                return ResponseEntity.ok().build();
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }
}
